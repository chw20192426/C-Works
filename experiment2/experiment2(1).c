#include <stdio.h>
int main()
{
    printf("输入多个正整数，以-1结束，输出最大值。\n");

    int a = 0, b = 0;

    while (a + 1)                               //此代码块的作用是比较输入的所有数的大小并把最大的数储存到b中
    {
        scanf("%d", &a);                        //scanf遇到空格就不再读取，这里采用scanf将输入的数分割
        if (b <= a)
        {
            b = a;
        }
    }
    printf("输入的所有正整数的最大值为%d", b);
    return 0;
}