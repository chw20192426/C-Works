#include <stdio.h>
#include <string.h>
int RemoveSame(char a[][20], int d, char b[][20], int e, char c[][20]);
int sort(char a[][20], int m);

int main()
{
    int d = 0, e = 0, count;
    char a[50][20] = {}, b[50][20] = {}, c[100][20] = {}, m;
    printf("请输入A数组的内容(以空格分隔)：");
    do
    {
        scanf("%s", &a[d]);
        d += 1;
    } while ((m = getchar()) != '\n');  //当换行时，循环结束，A数组内容读取结束
    printf("请输入B数组的内容(以空格分隔)：");
    do
    {
        scanf("%s", &b[e]);
        e += 1;
    } while ((m = getchar()) != '\n');  //当换行时，循环结束，B数组内容读取结束
    count = RemoveSame(a, d, b, e, c);
    sort(c, count);
    printf("合并后的数组：");
    for (int i = 0; i < count; i++)     //通过循环输出合并后的数组的内容
    {
        if (i == 0)
        {
            printf("{%s,", c[i]);
        }
        else if (i == count - 1)
        {
            printf("%s}", c[i]);
        }
        else
        {
            printf("%s,", c[i]);
        }
    }

    return 0;
}

int RemoveSame(char a[][20], int d, char b[][20], int e, char c[][20])
{
    //函数功能：去掉字符数组中重复出现的字符串
    int judge, m = 1;
    for (int i = 0; i < d; i++)
    {
        if (i == 0)
        {
            sprintf(c[i], a[i]);    //sprintf()函数的作用：将a[i]的内容输出到c[i]中
        }
        else
        {
            for (int j = 0; j < i; j++) //构建循环，检查a[i]的内容是否在数组c中出现过
            {
                if (strcmp(c[j], a[i]) == 0)    //如果重复，循环结束，judge+1
                {
                    judge += 1;
                    break;
                }
            }
            if (judge == 0)
            {
                sprintf(c[m], a[i]);    //若没有出现过，则将a[i]的值输出到数组c中
                m++;
            }
            else
            {
                judge = 0;              //若出现过，judge归零，继续循环
                continue;
            }
        }
    }
    for (int i = 0; i < e; i++)         //继续构建循环，检查b[i]的内容是否在数组c中出现过
    {
        for (int j = 0; j < m + i; j++)
        {
            if (strcmp(c[j], b[i]) == 0)    //如果重复，循环结束，judge+1
            {
                judge += 1;
                break;
            }
        }
        if (judge == 0)
        {
            sprintf(c[m], b[i]);        //若没有出现过，则将a[i]的值输出到数组c中
            m++;
        }
        else
        {
            judge = 0;                  //若出现过，judge归零，继续循环
            continue;
        }
    }

    return m;
}
int sort(char a[][20], int m)
{
    char tmp[20];
    for (int i = 0; i < m; i++)     //使用冒泡排序，将字符串按ASCII码从小到大排序
    {
        for (int j = 0; j < m - i - 1; j++)
        {
            if (strcmp(a[j], a[j + 1]) > 0)
            {
                sprintf(tmp,a[j]);
                sprintf(a[j],a[j + 1]);
                sprintf(a[j + 1],tmp);
            }
        }
    }
}