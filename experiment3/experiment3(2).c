#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
int random(char mark)   //读取运算符，生成两个20以内的随机数，且计算结果也在20以内
{
    int a, b;
    if (mark == '+')    
    {
        while (1)
        {
            srand(time(0));
            a = rand() % 20;    //生成两个20以内的随机数
            b = rand() % 20;
            if (a + b <= 20)
            {
                printf("%d+%d=", a, b); //输出问题
                return a + b;   //返回正确答案
            }
        }
    }
    if (mark == '-')
    {
        while (1)
        {
            srand(time(0));
            a = rand() % 20;    //生成两个20以内的随机数
            b = rand() % 20;
            if (a - b >= 0)
            {
                printf("%d-%d=", a, b); //输出问题
                return a - b;   //返回正确答案
            }
        }
    }
}

int judge(int answer,int correct)   //判断输入的答案是否正确
{
    if (answer==correct)
    {
        printf("That’s right!\n");
        return 1;                   //答案正确，返回1
    }
    else
    {
        printf("It’s not correct!\n");
        return 0;                   //答案错误，返回0
    }
}

int result(int total,int right,int wrong,int score) //测试完成，输出统计数据
{
    printf("测试完成！本次测试情况如下：\n");
    printf("共完成：%d\n正确：%d\n错误：%d\n总分：%d\n",total,right,wrong,score);   
}

int main()
{
    int total=0,right=0,wrong=0,correct,answer,score=0;
    char mark,more[4];
    printf("请选择加法运算或减法运算（输入+或-）:\n");
    scanf("%c",&mark);  //读取运算符
    for (int i = 0; i < 100; i++)   //构造循环，使单次最大答题数为100
    {
        correct=random(mark);
        scanf("%d",&answer);
        if (judge(answer,correct))  //若答案正确，正确题目数+1，分数+10
        {
            right++;
            score+=10;
        }
        else
        {
            wrong++;                //答案错误，错误题目数+1
        }
        total++;                    //总作答数+1

        printf("目前得分：%d\n",score);
        printf("是否继续做题?(yes或no)\n"); //判断是否继续做题
        scanf("%s",&more);
        if (strcmp(more,"no")==0||strcmp(more,"yes")!=0&&strcmp(more,"no0")!=0)
        {
            if (strcmp(more,"no")!=0&&strcmp(more,"yes")!=0)
            {
                printf("输入错误，程序退出！\n");
            }
            break;
        }
    }
    result(total,right,wrong,score);    //输出最终结果
    
    return 0;
}