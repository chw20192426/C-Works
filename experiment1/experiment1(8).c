#include <stdio.h>
int main()
{
    printf("输入一个字符，输出它的下一个字符；\n");

    char a;
    printf("请输入一个字符:\n");
    scanf("%c", &a);       //输入字符
    printf("%c\n", a + 1); //a+1的实质是将其ASCII码的值加一，因而可以输出下一个字符
    return 0;
}
