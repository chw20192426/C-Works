#include <stdio.h>
int judgeprime(int n)	//判断素数，若为素数，函数返回1，否则返回0
{
	int i;
	for (int i = 2; i * i <= n; i++)
	{
		if (n % i == 0)
		{
			return 0;
		}
	}
	return n > 1;
}

int main()	//输出1000之内的所有素数
{
	int i, count = 0;
	for (i = 1; i <= 1000; i++)	//构造循环进行打印
	{
		if (judgeprime(i))	//若为素数则打印出来
		{
			printf("%d\t", i);
			count++;	//计数器加一
		}
		if (count == 6)	//当计数器等于6的倍数时，打印回车，并将计数器复位
		{
			count = 0;
			printf("\n");
		}
	}

	return 0;
}
