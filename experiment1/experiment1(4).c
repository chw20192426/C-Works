#include <stdio.h>
#include <math.h>
int main()
{
	printf("输入实数x，计算(e^x-e^(-x))/(e^x+e^(-x))，精度保留5位。注意这里的^表示多少次方，C语言里没有次方操作符\n");

	double x, result;
	printf("请输入一个实数：\n");
	scanf("%lf", &x);
	double x1 = exp(x) - exp(-x); //计算分子,exp(n)函数计算e的n次方的值
	double x2 = exp(x) + exp(-x); //计算分母

	result = x1 / x2; //计算结果
	printf("%.5lf", result);
	return 0;
}
