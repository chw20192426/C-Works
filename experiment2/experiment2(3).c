#include <stdio.h>
#include <math.h>
int main()
{
    printf("求满足x^2+5x+7为23倍数的所有的x（x取值在0和22之间）\n");

    int x, sum, i=0;
    int y[23] = {0};


    for (x = 0; x < 23; x++)            //分别让x的值从0取到22，判断结果是否为23的倍数
    {
        sum = pow(x, 2) + 5 * x + 7;
        if (sum % 23 == 0)
        {
            y[i] = x;
            i++;
        }
    }
    if (y[0] == 0)                      //当y的第一个位置的值为0时，没有满足题意的x
    {
        printf("无解。\n");
    }
    else                                
    {
        int j = 0;
        for (i = 0; i < 23; i++)        //当满足题意时，判断满足的x的个数
        {
            if (y[i] != 0)
            {
                y[j] = y[i];
                j++;
            }
        }
        for (int i = 0; i < j; i++)     //当满足题意时，输出相应的x
        {
            printf("%d ", y[i]);
        }
    }
    return 0;
}