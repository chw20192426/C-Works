#include <stdio.h>
#include <math.h>

//四舍五入
int main()
{
	float i;
	scanf("%f", &i);
	//方法一：int a = (i+0.5);
	/*方法二：int a = round(i);
	printf("%d", a);*/
	printf("%.0f",i);//方法三
	return 0;
}
