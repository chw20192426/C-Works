#include <stdio.h>
float average(int a[]);

int main()
{
    int n, i, j;
    float avg, sum[10];
    printf("请输入歌手人数：");
    scanf("%d", &n);            //读取歌手人数
    int a[n][10];
    printf("请输入各选手得分（不同评委的分数用空格分隔，不同歌手的分数用换行分隔）：");
    for (i = 0; i < n; i++)     //通过二重循环读取分数信息
    {
        for (j = 0; j < 10; j++)
        {
            scanf("%d", &a[i][j]);
        }
        sum[i] = average(a[i]); //当输入完一个歌手的数据后，计算该组数据的平均值
    }
    for (i = 0; i < n; i++)
    {
        printf("第%d位歌手的平均分为：%g\n", i + 1, sum[i]);
    }

    return 0;
}

float average(int a[])
{
    //函数功能：去除最大值、最小值求平均数
    int max, min, sum = 0;
    float avg;
    for (int i = 0; i < 10; i++)    //通过循环来找出该组数据中的最大值和最小值
    {
        if (i == 0)
        {
            max = a[0];
            min = a[0];
        }
        else if (a[i] > max)
        {
            max = a[i];
        }
        else if (a[i] < min)
        {
            min = a[i];
        }
        sum += a[i];
    }
    sum = sum - max - min;          //减去最大值、最小值
    avg = sum / 8.0;
    return avg;                     //返回平均数
}
