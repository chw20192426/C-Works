#include <stdio.h>
#include <math.h>
int main()
{
    int a;
    printf("请输入一个整数:");
    scanf("%d", &a);
    if (a >= 0)
    {
        float b = sqrt(a);
        printf("该整数的平方根为%f", b);
    }
    else
    {
        printf("负数在实数范围内的平方根不存在");
    }
    return 0;
}