#include <stdio.h>
#include <string.h>
int main()
{
    printf("输入一行字符，输出每个字符的ASCII码（十进制，用空格隔开）\n");
    
    int i;
    char str[100] = "";                 
    printf("请输入一行字符:\n");

    scanf("%s", &str);                      //读取输入的字符串
    for (i = 0; i < strlen(str); i++)
    {
        printf("%d ", str[i]);              //使用循环分别输出每个字符的ASCII码
    }
    return 0;
}
/*
#include <stdio.h>
int main()
{
    printf("输入一行字符，输出每个字符的ASCII码（十进制，用空格隔开）\n");
    
    int i=-1,j;
    char str[100] = "";
    printf("请输入一行字符:\n");

    while(str[i]!='\n'){
        scanf("%c",&str[i+1]);
        i++;
    }
    for (j = 0; j < i; j++)
    {
        printf("%d ", str[j]);
    }
    return 0;
}
*/