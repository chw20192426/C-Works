#include <stdio.h>
#include <math.h>
int main()
{
    printf("输入一个角度（0-360度），计算其正弦函数值，精度保留5位\n");

    double angle;
    printf("请输入一个角度(0-360度):\n");
    scanf("%lf", &angle);                  //输入结果,注意double类型需要用%lf
    double PI = acos(-1.0);               //计算了π的大小，acos()函数是反余弦函数
    angle = angle / 180 * PI;             //将角度制转化为弧度制
    double result = sin(angle);           //计算结果
    printf("该角度正弦值为%.5lf", result); //输出结果

    return 0;
}
