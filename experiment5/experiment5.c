#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct student stud;
struct student //定义学生信息结构体
{
    char ID[9];
    char name[16];
    float grade[5];
};

void student_info(stud *stu, int n);
int judge(stud *stu, int n, int mode);
void sort(stud *stu, int n, int sum);
void search_id(stud *stu, int n);
void search_class(stud *stu, int n);
void search_grade(stud *stu, int n);
void search_agrade(stud *stu, int n);
void transcript(stud *stu, int n);

int main()
{
    int n, i, mode, a = 0, choose;
    printf("请输入尚未录入成绩学生的个数:");
    scanf("%d", &n);
    stud *stu;                              //定义结构体指针
    stu = (stud *)malloc(sizeof(stud) * n); //为定义的指针在内存中分配相应大小的空间
    student_info(stu, n);                   //调用函数来接收输入的学生信息
    while (n)
    {
        printf("\n================================================");
        printf("\n现在进入成绩查询模式（输入对应的编号进行查询）:\n");
        printf("1.根据学号或姓名查询某个学生成绩\n");
        printf("2.查询某门课第n-m名学生成绩\n");
        printf("3.查询某门课分数在n-m之间的学生成绩\n");
        printf("4.查询某门课平均成绩\n");
        scanf("%d", &mode);      //确定进入的模式
        a = judge(stu, n, mode); //调用函数来进入输入的数字对应的模式，模式存在返回1，不存在返回0
        if (a == 0)              //如果输入的模式不存在，循环直到输入正确
        {
            continue;
        }
        while (1)
        {
            printf("是否继续查询(1 or 0)？\n");
            scanf("%d", &choose);
            if (choose != 1 && choose != 0)
            {
                printf("\033[31m请检查输入的是否为0或1，之后重新输入序号！\033[0m\n");
            }
            else
            {
                break;
            }
        }
        if (choose == 0)
        {
            transcript(stu, n); //若不再继续查询，调用函数将学生的成绩列表输出
            break;
        }
    }
    if (n == 0)
    {
        printf("尚无需要录入的数据，程序退出。"); //若未录入的学生人数为0，打印信息
    }

    free(stu); //释放内存空间
    return 0;
}

void student_info(stud *stu, int n)
{
    //输入学生个数，录入学生信息
    int i, j;
    for (i = 0; i < n; i++)
    {
        printf("请输入学生姓名、学号(以空格分隔):");
        scanf("%s%s", stu[i].name, &stu[i].ID);
        printf("请分别输入该生的五科成绩(以空格分隔):");
        for (j = 0; j < 5; j++)
        {
            scanf("%f", &stu[i].grade[j]);
        }
    }
}
int judge(stud *stu, int n, int mode)
{
    //索引函数，判断mode的值并调用对应的函数，若mode不存在则返回0，存在则返回1
    switch (mode)
    {
    case 1:
        search_id(stu, n);
        return 1;
    case 2:
        search_class(stu, n);
        return 1;
    case 3:
        search_grade(stu, n);
        return 1;
    case 4:
        search_agrade(stu, n);
        return 1;
    default:
        printf("\033[31m请检查输入的序号是否正确，之后重新输入序号！\033[0m\n");
        return 0;
    }
}
void sort(stud *stu, int n, int num)
{
    //将结构体数组按照给定的一科学生成绩由高到低冒泡排序
    stud tmp;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (stu[j].grade[num - 1] < stu[j + 1].grade[num - 1])
            {
                tmp = stu[j];
                stu[j] = stu[j + 1];
                stu[j + 1] = tmp;
            }
        }
    }
}
void search_id(stud *stu, int n)
{
    // 根据学号或姓名查询某个学生成绩
    int count = 0;
    char a[20] = "";
    printf("请输入学生的姓名或学号:");
    scanf("%s", &a);
    for (int i = 0; i < n; i++)
    {
        if (strcmp(stu[i].name, a) == 0 || strcmp(stu[i].ID, a) == 0)
        {
            printf("姓名:%s\t学号:%s\t", stu[i].name, stu[i].ID);
            printf("该生五科成绩:\t");
            for (int j = 0; j < 5; j++)
            {
                printf("%g\t", stu[i].grade[j]);
            }
            printf("\n");
            count++;
            break;
        }
    }
    if (count == 0)
    {
        printf("未找到对应的结果！\n");
    }
}
void search_class(stud *stu, int n)
{
    //查询某门课第n-m名学生成绩
    int a, b, num, tmp, count = 0;
    printf("请输入想要查询的课程编号（1——5）:");
    scanf("%d", &num);
    sort(stu, n, num);
    printf("请输入学生名次查询范围n——m(0<n<=m<%d)，以空格分隔:", n);
    scanf("%d%d", &a, &b);
    if (a > b) //防止输入错误
    {
        tmp = a;
        a = b;
        b = tmp;
    }
    for (int i = a - 1; i < b; i++)
    {
        printf("%d.姓名:%s\t学号:%s\n", i + 1, stu[i].name, stu[i].ID);
        printf("该生本门课成绩:");
        printf("%g\n", stu[i].grade[num - 1]);
        count++;
    }
    if (count == 0)
    {
        printf("未找到对应的结果！\n");
    }
}
void search_grade(stud *stu, int n)
{
    //查询某门课分数在n-m之间的学生成绩
    int a, b, num, tmp, count = 0;
    printf("请输入想要查询的课程编号（1——5）:");
    scanf("%d", &num);
    printf("请输入分数查询范围n——m(0<n<=m<=100)，以空格分隔:");
    scanf("%d%d", &a, &b);
    if (a > b) //防止输入错误
    {
        tmp = a;
        a = b;
        b = tmp;
    }
    for (int i = 0, j = 1; i < n; i++)
    {
        if (stu[i].grade[num - 1] >= a && stu[i].grade[num - 1] <= b)
        {
            printf("%d.姓名:%s\t学号:%s\n", j, stu[i].name, stu[i].ID);
            printf("该生本门课成绩:");
            printf("%g\n", stu[i].grade[num - 1]);
            j++;
            count++;
        }
    }
    if (count == 0)
    {
        printf("未找到对应的结果！\n");
    }
}
void search_agrade(stud *stu, int n)
{
    //查询某门课平均成绩
    int num;
    float sum = 0, avg;
    printf("请输入想要查询的课程编号（1——5）:");
    scanf("%d", &num);
    for (int i = 0; i < n; i++)
    {
        sum += stu[i].grade[num - 1];
    }
    avg = sum / n;
    printf("该门课的平均成绩为%g\n", avg);
}
void transcript(stud *stu, int n)
{
    //按照各门课平均成绩从高到低输出所有学生的成绩表
    float sum[n], tmp1;
    stud tmp;
    printf("查询完毕，现将所有学生的成绩列表输出:\n");
    for (int i = 0; i < n; i++) //计算每个学生的总成绩
    {
        sum[i] = stu[i].grade[0];
        for (int j = 1; j < 5; j++)
        {
            sum[i] += stu[i].grade[j];
        }
    }
    for (int i = 0; i < n; i++) //将结构体数组按照总成绩由高到低进行排序
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (sum[j] < sum[j + 1])
            {
                tmp = stu[j];
                stu[j] = stu[j + 1];
                stu[j + 1] = tmp;
                tmp1 = sum[j];
                sum[j] = sum[j + 1];
                sum[j + 1] = tmp1;
            }
        }
    }
    for (int i = 0; i < n; i++) //按照成绩的排名输出成绩表
    {
        printf("第%d名    姓名:%s\t学号:%s\t平均成绩:%g\t", i + 1, stu[i].name, stu[i].ID, sum[i] / 5);
        printf("该生五科成绩:\t");
        for (int j = 0; j < 5; j++)
        {
            printf("%g\t", stu[i].grade[j]);
        }
        printf("\n");
    }
}