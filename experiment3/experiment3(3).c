#include <stdio.h>
int factorial(int n){   //利用递归计算阶乘
    if (n==1)
    {
        return 1;
    }
    return n*factorial(n-1);
}

int combinatorial(int n,int m){ //通过组合数的定义计算组合数
    int sum;
    sum=factorial(n)/(factorial(m)*factorial(n-m));
    return sum;
}

int main(){
    int n,m,sum,swp;
    printf("请输入组合数C(n,m)中n、m的值(0<=m<=n<=10)：");
    scanf("%d%d",&n,&m);    //读取输入的n、m
    if (n<m)    //若m>n,应当对m，n的值进行交换再运算组合数
    {   
        printf("\033[31m输入数据错误，已交换m、n的值！\033[0m\n");
        swp = n;
        n = m;
        m = swp;
    }
    sum = combinatorial(n,m);   //通过函数计算组合数
    printf("组合数C(n,m)的大小为：%d",sum); //输出计算结果
    return 0;
}