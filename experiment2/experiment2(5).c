#include <stdio.h>
int main()
{
    printf("输入年，输出这一年每个月的天数（注意判断闰年）\n");

    int year;
    printf("请输入年份：\n");
    scanf("%d", &year);
    printf("各月天数如下：\n");
    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)    //判断闰年，若是闰年则2月有29天
    {
        printf("一月：31天\n");
        printf("二月：29天\n");
        printf("三月：31天\n");
        printf("四月：30天\n");
        printf("五月：31天\n");
        printf("六月：30天\n");
        printf("七月：31天\n");
        printf("八月：31天\n");
        printf("九月：30天\n");
        printf("十月：31天\n");
        printf("十一月：30天\n");
        printf("十二月：31天\n");
    }
    else                                                        //若不是闰年，则2月有28天
    {
        printf("一月：31天\n");
        printf("二月：28天\n");
        printf("三月：31天\n");
        printf("四月：30天\n");
        printf("五月：31天\n");
        printf("六月：30天\n");
        printf("七月：31天\n");
        printf("八月：31天\n");
        printf("九月：30天\n");
        printf("十月：31天\n");
        printf("十一月：30天\n");
        printf("十二月：31天\n");
    }

    return 0;
}