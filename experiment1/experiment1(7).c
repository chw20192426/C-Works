#include <stdio.h>
#define PI 3.1415926 //宏定义，定义π的大小，这里由于结果的精度限制，不需要π的确切数值
int main()
{
	printf("输入半径和高，输出圆柱体的体积，精度保留2位\n");

	float r, h;
	printf("请输入圆柱体的半径和高,以空格分隔:\n");
	scanf("%f%f", &r, &h);					//输入半径和高
	float result = PI * r * r * h;			//计算体积
	printf("圆柱体体积为：%.2f\n", result); //输出结果
	return 0;
}
