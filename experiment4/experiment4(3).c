#include <stdio.h>
int RemoveSame(int a[], int b[], int i);
int sort(int a[], int n);

int main()
{
    int a[20] = {0}, b[20] = {0}, n, i = 0;
    char y;
    printf("请输入总数不超过20个的整数（以空格分隔）：");
    do
    {
        scanf("%d", &a[i]);
        if (i >= 20)                        //防止数组下标越界
        {
            break;
        }
        i++;
    } while ((y = getchar()) != '\n');      //当换行时，循环结束
    n = RemoveSame(a, b, i);
    sort(b, n);
    printf("去重排序后的结果为：");
    for (int i = 0; i < n; i++)             //构建循环输出去重、排序后的结果
    {
        printf("%d ", b[i]);
    }

    return 0;
}

int RemoveSame(int a[], int b[], int i)
{
    //函数功能：去掉数组中重复出现的数
    int n = i, judge = 0, m = 1;
    for (int i = 0; i < n; i++)
    {
        if (i == 0)
        {
            b[i] = a[i];
        }
        else
        {
            for (int j = 0; j < i; j++) //构建循环，检查a[i]的内容是否在数组b中出现过
            {
                if (b[j] == a[i])       //如果重复，循环结束，judge+1
                {
                    judge += 1;
                    break;
                }
            }
            if (judge == 0)
            {
                b[m] = a[i];            //若没有出现过，则将a[i]的值输出到数组b中
                m++;
            }
            else
            {
                judge = 0; //debug
                continue;               //若出现过，则继续循环
            }
        }
    }
    return m;
}
int sort(int a[], int n)
{
    int tmp;
    for (int i = 0; i < n; i++)         //使用冒泡排序将数组从大到小排序
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (a[j] < a[j + 1])
            {
                tmp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = tmp;
            }
        }
    }
}