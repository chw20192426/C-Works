//将矩阵的两行作交换输出
#include <stdio.h>
int main()
{
    int n, a, b;
    int num[21][21];
    int i, j, temp;
    scanf("%d%d%d", &n, &a, &b);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            scanf("%d", &num[i][j]);
        }
    }
    for (i = 0; i < n; i++)
    {
        temp = num[a - 1][i];
        num[a - 1][i] = num[b - 1][i];
        num[b - 1][i] = temp;
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (j==n-1)
            {
                printf("%d", num[i][j]);
            }
            else
            {
                printf("%d ", num[i][j]);
            }
            
            
        }
        printf("\n");
    }
    return 0;
}