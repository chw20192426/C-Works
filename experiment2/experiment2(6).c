#include <stdio.h>
#include <math.h>
int main()
{
    printf("输入n，输出1——n的立方和\n");

    int n, sum,i;
    scanf("%d", &n);                    //读取n的值
    for (i = 0; i <= n; i++)
    {
        sum = sum + pow(i, 3);          //使用循环计算立方和
    }
    printf("所求立方和为%d", sum);

    return 0;
}