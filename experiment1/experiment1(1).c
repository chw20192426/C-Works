#include <stdio.h>

int main()
{
	printf("输入5个整数，输出其平均值，精度保留1位\n");

	int a1, a2, a3, a4, a5;

	printf("请输入五个整数，用空格分隔:\n");
	scanf("%d%d%d%d%d", &a1, &a2, &a3, &a4, &a5); //输入5个数，分别保存在五个变量中

	float average = (a1 + a2 + a3 + a4 + a5) / 5;		  //计算平均数
	printf("求得平均数为\033[32m%.1f\033[0m\n", average); //输出平均数（\033可以控制输出字符的颜色）

	return 0;
}
