#include <stdio.h>
#include <math.h>
int main()
{
	printf("输入实数x，计算x^5+3x^3+2.6x^2+1，精度保留3位\n");

	printf("请输入一个实数:\n");
	double x;
	scanf("%f", &x); //输入结果

	double result = pow(x, 5) + 3 * pow(x, 3) + 2.6 * pow(x, 2) + 1; //pow(x,y)函数可以计算x的y次方

	printf("结果为%.3f", result); //输出结果
	return 0;
}
