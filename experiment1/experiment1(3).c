#include <stdio.h>
int main()
{
	printf("已知2019年1月1日是星期二，计算2020年1月1日是星期几（提示：利用1年多少天，7天一个星期）\n");
	int origin = 2, result;
	int day = 365;
	int mod = day % 7; //计算365天除以7的余数
	if (mod == 6)
	{ //当余数为6时，表示星期一，result应为1
		result = 1;
	}
	else
	{ //其他情况，result的值是原来的星期二加上余数后的值
		result = origin + mod;
	}
	switch (result)
	{ //根据result数值输出具体的日期
	case 1:
		printf("2020年1月1日是星期一");
		break;
	case 2:
		printf("2020年1月1日是星期二");
		break;
	case 3:
		printf("2020年1月1日是星期三");
		break;
	case 4:
		printf("2020年1月1日是星期四");
		break;
	case 5:
		printf("2020年1月1日是星期五");
		break;
	case 6:
		printf("2020年1月1日是星期六");
		break;
	case 7:
		printf("2020年1月1日是星期日");
		break;
	}
	return 0;
}
