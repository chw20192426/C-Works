#include <stdio.h>
int main()
{
    printf("输入一个字符，输出其ASCII码的十进制和十六进制表示（提示：用%02x）\n");

    char a;
    printf("请输入一个字符:\n");
    scanf("%c", &a);                              //输入字符
    printf("它的ASCII码的十进制表示为%d\n", a);   //使用%d格式化输出a实质上输出了a的ASCII码的十进制值
    printf("它的ASCII码的十六进制表示为%02x", a); //使用%x输出a实质上输出了a的ASCII码的十六进制值，02表示宽度为2，若宽度不满2，则在前面用0补全
    return 0;
}
