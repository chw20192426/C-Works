#include <stdio.h>

int main()
{
    printf("输入一个5位整数，利用运算符获得并输出它的各位数字，每行一个数字\n");

    int b1, b2 = 10000;

    printf("请输入一个五位数:");
    scanf("%d", &b1);
    printf("该数字的每一位为（由高位到低位）:\n");
    while (b2)
    {
        int b = b1 / b2;                  //计算出了数字的最高位的数
        printf("\033[32m%d\n\033[0m", b); //\033可以控制输出字符串的颜色,\n表示换行
        b1 = b1 % b2;                     //将输入数字的最高位数舍去，使最高位的下一位成为成为最高位
        b2 /= 10;                         //使b2适应b1的变化
    }

    return 0;
}