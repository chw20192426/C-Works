#include <stdio.h>
#include <math.h>
int main()
{
    printf("先录入一个关于x的5次多项式的系数，然后反复输入x的值，输出多项式的值（小数点后保留两位数字），当输入的x大于10^6时退出。\n");

    float a, b, c, d, e, f, x;
    double sum;
    printf("请输入一个关于x的5次多项式的系数(以空格分隔，由最高次系数开始)：\n");
    scanf("%f%f%f%f%f%f", &a, &b, &c, &d, &e, &f);                                              //分别读取系数
    while (1)                                                                                   //建立循环，以便计算可以重复进行                                                                        
    {
        printf("请输入x的值：\n");
        scanf("%f", &x);
        if (x > 1000000)                                                                        //当x>10^6时，退出
        {
            break;
        }
        else                                                                                    //使用pow()函数求多项式的值，将结果保存在sum中
        {
            sum = a * pow(x, 5) + b * pow(x, 4) + c * pow(x, 3) + d * pow(x, 2) + e * x + f;
            printf("该多项式的值为%.2lf\n", sum);
        }
    }
    return 0;
}