#include <stdio.h>
int main()
{
    float x1, x2, y1, y2, x, y;
    printf("注意，所有输入的坐标需采用'x,y'的形式输入\n");

    printf("构建一个矩形区域。\n请输入左下角的坐标:");
    scanf("%f,%f", &x1, &y1);
    printf("请输入右上角的坐标:");
    scanf("%f,%f", &x2, &y2);

    if (x1 < x2 && y1 < y2)
    {
        printf("请输入一个点的坐标，我来判断它是否在构建的区域内:");
        scanf("%f,%f", &x, &y);                         //取地址符不敢再忘了，否则会产生段错误
        if (x > x1 && x < x2 && y > y1 && y < y2)
        {
            printf("\033[32m该点在构造的区域内。\033[0m\n");
        }
        else
        {
            printf("\033[32m该点不在构造的区域内。\033[0m\n");
        }
    }
    else
    {
        printf("\033[31m输入错误，请检查该区域是否存在。\033[0m\n");
    }
    return 0;
}