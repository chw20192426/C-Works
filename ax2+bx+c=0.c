#include <stdio.h>
int main()
{
    float a, b, c;
    printf("请输入一元二次方程的系数a,b,c(以空格分隔):");
    scanf("%f%f%f", &a, &b, &c);
    float delta = b * b - 4 * a * c;
    if (a == 0)
    {
        printf("输入的方程不是一元二次方程");
    }
    else
    {
        if (delta > 0)
        {
            printf("该方程在实数范围内有两个解。\n");
        }
        else if (delta == 0)
        {
            printf("该方程在实数范围内有一个解。\n");
        }
        else
        {
            printf("该方程在实数范围内无解。\n");
        }
    }
    return 0;
}